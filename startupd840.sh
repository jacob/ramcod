#!/bin/bash
sudo apt-get update
sudo apt-get -y install python3-pip

sudo apt install -ylibfftw3-dev cmake pkg-config libliquid-dev python3-tk libuhd-dev uhd-host mdadm feh build-essential git inspectrum gnuradio htop uhd-host

sudo pip3 install beautifulsoup4

sudo sysctl -w net.core.rmem_max=24862979
sudo sysctl -w net.core.wmem_max=24862979

sudo mdadm --create --verbose /dev/md/nvme --level=0 --raid-devices=4 /dev/nvme0n1 /dev/nvme1n1 /dev/nvme2n1 /dev/nvme3n1
sudo mkfs.ext4 -F /dev/md/nvme
sudo mkdir /nvme
sudo mount /dev/md/nvme /nvme/
sudo chmod 777 /nvme

sudo fstrim /nvme/

v=$(netstat -ie | grep -B1 "192.168.40.1 " | head -n 1 | awk '{print $1}')

interface=${v%:}

sudo ip link set mtu 9000 dev ${interface} 

