#include <uhd.h>
#include "getopt.h"
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <pthread.h> 
#include <complex.h>
#include <string.h>
#include <math.h>
#include <fftw3.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

#define MAGIC 1234567890
#define FFTSIZE 1024
#define BUFFSIZE 1024

typedef unsigned int uint;

pthread_mutex_t fftwLock; //Mutex lock for fftw
pthread_mutex_t sendLock; //Mutex lock for sending data

char *host = "localhost";
bool verbose = false;
int id = 1;
int port = 8080;
int groupSize = 10000;
double sampleRate = 50e6;
double centerFreq = 2835e6;
double gain = 0.0;

struct iq //Structure to hold short iq data from SDR
{
    float i;
    float q;
};

struct header //Structure to hold information about the data to be transmitted
{
    uint id; //The id of the sender
    uint length; //The length of the data array to be transmitted also the size of the fft
    uint size; //The number of samples this data represents
    uint sampleRate; //The sample rate of the collected data
    uint centerFreq; //The center frequency of the collected data
    uint gain; //The gain of the collected data
    uint magic; //The magic number to make sure the data is correct
};

struct header buildHeader(uint length, uint size)
{
    struct header info;
    info.id = getpid();
    info.length = length;
    info.size = size;
    info.sampleRate = sampleRate;
    info.centerFreq = centerFreq;
    info.gain = gain;
    info.magic = MAGIC;
    return info;
}

char *lookupHost (const char *host, char *addrstr)
{
  struct addrinfo hints, *res, *result;
  int errcode;
  void *ptr;

  memset (&hints, 0, sizeof (hints));
  hints.ai_family = PF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags |= AI_CANONNAME;

  errcode = getaddrinfo (host, NULL, &hints, &result);
  if (errcode != 0)
    {
      printf("Issue resolving hostname\n");
    }
  
  res = result;

  while (res)
    {
      inet_ntop (res->ai_family, res->ai_addr->sa_data, addrstr, 100);

      switch (res->ai_family)
        {
        case AF_INET:
          ptr = &((struct sockaddr_in *) res->ai_addr)->sin_addr;
          break;
        case AF_INET6:
          ptr = &((struct sockaddr_in6 *) res->ai_addr)->sin6_addr;
          break;
        }
      inet_ntop (res->ai_family, ptr, addrstr, 100);
      res = res->ai_next;
    }
  
  freeaddrinfo(result);

  return addrstr;
}

void sendResults(float *data)
{
  int status, client_fd;
	struct sockaddr_in serv_addr;
	struct header captureInfo = buildHeader(sizeof(fftw_complex) * FFTSIZE, FFTSIZE);
  pthread_mutex_lock(&sendLock); //Lock mutex with send access
	
  if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
    pthread_mutex_unlock(&sendLock); //Unlock mutex with send access
		return;
	}

	char addrstr[16]; // space to hold the IPv4 string "xxx.xxx.xxx.xxx"
	lookupHost(host, addrstr); // Lookup the IP address of the hostname
	if(verbose) printf("Host: %s\n", addrstr); // Report the IP address back to the user
	serv_addr.sin_family = AF_INET; // Use IPv4
	serv_addr.sin_port = htons(port); // Set the port to connect to

	// Convert IPv4 and IPv6 addresses from text to binary
	if (inet_pton(AF_INET, addrstr, &serv_addr.sin_addr) <= 0) {
		printf("\nInvalid address/ Address not supported \n");
    pthread_mutex_unlock(&sendLock); //Unlock mutex with send access
		return;
	}

	if ((status = connect(client_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)))< 0) 
    {
      printf("\nConnection Failed \n");
      pthread_mutex_unlock(&sendLock); //Unlock mutex with send access
		  return;
	}
    
	send(client_fd, &captureInfo, sizeof(struct header), 0); // Send the header to the server
  send(client_fd, data, sizeof(float) * FFTSIZE, 0); // Send the data to the server

	// closing the connected socket
	close(client_fd);
  pthread_mutex_unlock(&sendLock); //Unlock mutex with send access
}

void *calcfft(void *vargp) 
{ 
  struct iq *data = (struct iq *)vargp; //Cast void pointer to struct iq pointer
  fftw_complex in[FFTSIZE]; //Setup fft input array
  fftw_complex out[FFTSIZE]; //Setup fft output array
  float psd[FFTSIZE]; //Setup array for fft results
  float maxPSD[FFTSIZE]; //Setup array for max fft results from group results

  pthread_mutex_lock(&fftwLock); //Lock mutex with fftw access
  fftw_plan plan = fftw_plan_dft_1d(FFTSIZE, in, out, FFTW_FORWARD, FFTW_MEASURE); //Setup fft plan
  pthread_mutex_unlock(&fftwLock); //Unlock mutex with fftw access

  for(int i = 0; i < groupSize; i++){ //Calculate the fft of every FFTSIZE samples
    for(int j = 0; j < FFTSIZE; j++){ //Copy data from struct iq array to fftw_complex array
      in[j] = data[i * FFTSIZE + j].i + data[i * FFTSIZE + j].q * I; //Convert to complex double
    }

    fftw_execute(plan); //Execute fft

    for(int j = 0; j < FFTSIZE; j++){ //Calculate power spectral density
      psd[j] = (pow(abs(out[j]), 2) / sampleRate*FFTSIZE);

      if(i == 0){ //If this is the first fft, set the max psd to the current psd
        maxPSD[j] = psd[j];
      }
      else if (psd[j] > maxPSD[j]){ //If the current psd is greater than the max psd, replace it
        maxPSD[j] = psd[j];
      }
    }
  }

  free(vargp); //Free memory allocated for data since it is no longer needed

  //for(int i = 0; i < FFTSIZE; i++){ //Print the max psd
    //printf(" %f ", maxPSD[i]);
  //}

  sendResults(maxPSD); //Send results to client

  pthread_mutex_lock(&fftwLock); //Lock mutex with fftw access
  fftw_destroy_plan(plan); //Destroy fft plan
  pthread_mutex_unlock(&fftwLock); //Unlock mutex with fftw access

  pthread_exit(0); //Exit thread
}

#define EXECUTE_OR_GOTO(label, ...) \
    if (__VA_ARGS__) {              \
        return_code = EXIT_FAILURE; \
        goto label;                 \
    }

void print_help(void)
{
    fprintf(stderr,
        "radio - A program to collect IQ samples from UHD device and fowrward dBX/HZ values to a remote server\n\n"

        "Options:\n"
        "    -a (device args)\n"
        "    -f (frequency in Hz: Default 2835MHz)\n"
        "    -r (sample rate in Hz: Default 50MHz)\n"
        "    -g (gain: Default 0)\n"
        "    -s (group size: Default 10000)\n"
        "    -d (destination for radio samples: default localhost)\n"
        "    -p (destination port: Default 8080)\n"
        "    -v (enable verbose prints)\n"
        "    -h (print this help message)\n");
};

int main(int argc, char* argv[])
{
    int option           = 0;
    char* device_args    = NULL;
    size_t channel       = 0;
    char* filename       = "out.dat";
    int return_code      = EXIT_SUCCESS;
    char error_string[512];

    //Setup mutex locks
    if (pthread_mutex_init(&fftwLock, NULL) != 0) { 
        printf("\n fft mutex init has failed\n"); 
        exit(-1); 
    }

    if (pthread_mutex_init(&sendLock, NULL) != 0) { 
        printf("\n send mutex init has failed\n"); 
        exit(-1); 
    }

    // Process options
    while ((option = getopt(argc, argv, "s:a:f:r:g:d:p:vh")) != -1) {
        switch (option) {
          case 's':
	              groupSize = atoi(optarg);
		            break;

            case 'a':
                device_args = strdup(optarg);
                break;

            case 'f':
                centerFreq = atof(optarg);
                break;

            case 'r':
                sampleRate = atof(optarg);
                break;

            case 'g':
                gain = atof(optarg);
                break;

            case 'd':
                host = strdup(optarg);
                break;

	          case 'p':
	              port = atoi(optarg);
		            break;

            case 'v':
                verbose = true;
                break;

            case 'h':
                print_help();
                goto free_option_strings;

            default:
                print_help();
                return_code = EXIT_FAILURE;
                goto free_option_strings;
        }
    }

    if (!device_args)
        device_args = strdup("");

    // Create USRP
    uhd_usrp_handle usrp;
    fprintf(stderr, "Creating USRP with args \"%s\"...\n", device_args);
    EXECUTE_OR_GOTO(free_option_strings, uhd_usrp_make(&usrp, device_args))

    // Create RX streamer
    uhd_rx_streamer_handle rx_streamer;
    EXECUTE_OR_GOTO(free_usrp, uhd_rx_streamer_make(&rx_streamer))

    // Create RX metadata
    uhd_rx_metadata_handle md;
    EXECUTE_OR_GOTO(free_rx_streamer, uhd_rx_metadata_make(&md))

    // Create other necessary structs
    uhd_tune_request_t tune_request = {
        .target_freq     = centerFreq,
        .rf_freq_policy  = UHD_TUNE_REQUEST_POLICY_AUTO,
        .dsp_freq_policy = UHD_TUNE_REQUEST_POLICY_AUTO,
    };
    uhd_tune_result_t tune_result;

    uhd_stream_args_t stream_args = {.cpu_format = "fc32",
        .otw_format                              = "sc16",
        .args                                    = "",
        .channel_list                            = &channel,
        .n_channels                              = 1};

    uhd_stream_cmd_t stream_cmd = {.stream_mode = UHD_STREAM_MODE_START_CONTINUOUS,//Continuous streaming mode
        .stream_now                             = true};

    size_t samps_per_buff;
    float* buff          = NULL;
    void** buffs_ptr     = NULL;
    FILE* fp             = NULL;
    size_t num_acc_samps = 0;

    // Set rate
    fprintf(stderr, "Setting RX Rate: %f...\n", sampleRate);
    EXECUTE_OR_GOTO(free_rx_metadata, uhd_usrp_set_rx_rate(usrp, sampleRate, channel))

    // See what rate actually is
    EXECUTE_OR_GOTO(free_rx_metadata, uhd_usrp_get_rx_rate(usrp, channel, &sampleRate))
    fprintf(stderr, "Actual RX Rate: %f...\n", sampleRate);

    // Set gain
    fprintf(stderr, "Setting RX Gain: %f dB...\n", gain);
    EXECUTE_OR_GOTO(free_rx_metadata, uhd_usrp_set_rx_gain(usrp, gain, channel, ""))

    // See what gain actually is
    EXECUTE_OR_GOTO(free_rx_metadata, uhd_usrp_get_rx_gain(usrp, channel, "", &gain))
    fprintf(stderr, "Actual RX Gain: %f...\n", gain);

    // Set frequency
    fprintf(stderr, "Setting RX frequency: %f MHz...\n", centerFreq / 1e6);
    EXECUTE_OR_GOTO(free_rx_metadata,
        uhd_usrp_set_rx_freq(usrp, &tune_request, channel, &tune_result))

    // See what frequency actually is
    EXECUTE_OR_GOTO(free_rx_metadata, uhd_usrp_get_rx_freq(usrp, channel, &centerFreq))
    fprintf(stderr, "Actual RX frequency: %f MHz...\n", centerFreq / 1e6);

    // Set up streamer
    stream_args.channel_list = &channel;
    EXECUTE_OR_GOTO(
        free_rx_streamer, uhd_usrp_get_rx_stream(usrp, &stream_args, rx_streamer))

    // Set up buffer
    EXECUTE_OR_GOTO(
        free_rx_streamer, uhd_rx_streamer_max_num_samps(rx_streamer, &samps_per_buff))
    if(samps_per_buff > BUFFSIZE){
        samps_per_buff = BUFFSIZE;
    }
    fprintf(stderr, "Buffer size in samples: %zu\n", samps_per_buff);
    buff      = (float *)calloc(FFTSIZE * groupSize, sizeof(float) * 2);//Allocate memory for data
    buffs_ptr = (void**)&buff;

    // Issue stream command
    fprintf(stderr, "Issuing stream command.\n");
    EXECUTE_OR_GOTO(
        free_buffer, uhd_rx_streamer_issue_stream_cmd(rx_streamer, &stream_cmd))

    // Actual streaming
    int count = 0; //Store total number of samples
    float *data; //Store data to be sent to client
    int64_t full_secs = 0;
    double frac_secs = 0;
    bool pastError = false;

    while (true) {
        size_t num_rx_samps = 0;
        data = buff + count * 2; //Set data to point to the next available spot in buff
        buffs_ptr = (void**) &data;
        //printf("data: %p\n", data);
        EXECUTE_OR_GOTO(close_file, uhd_rx_streamer_recv(rx_streamer, buffs_ptr, samps_per_buff, &md, 3.0, false, &num_rx_samps))

        uhd_rx_metadata_error_code_t error_code;
        EXECUTE_OR_GOTO(close_file, uhd_rx_metadata_error_code(md, &error_code))
        if (error_code != UHD_RX_METADATA_ERROR_CODE_NONE) {//If there is an error, print the error and discard the data
          fprintf(stderr, "Error code 0x%x was returned during streaming. ", error_code);
          pastError = true;//Note that an error has occured
        }

        else if(num_rx_samps != samps_per_buff){//If the number of samples received is not the same as the number of samples requested, print the error and discard the data
          fprintf(stderr, "Requested %zu samples, received %zu.\n", samps_per_buff, num_rx_samps);
          pastError = true;//Note that an error has occured
        }

        else{//If there is no error, continue
          int64_t old_secs = full_secs;
          double old_frac_secs = frac_secs;
	        uhd_rx_metadata_time_spec(md, &full_secs, &frac_secs);//Get the time the data was received

          if(pastError){//If there was a past error, print the error resolved message
            float secs = (full_secs - old_secs) + (frac_secs - old_frac_secs);
            fprintf(stderr, "Lost: %f secs\n", secs);
            pastError = false;//Note that the error has been resolved
          }

          count += samps_per_buff; //Increment count

          for(int i = 0; i < samps_per_buff; i++){//Check if the data is valid
            if(data[i] > 1 || data[i] < -1){//If the data is not valid, print the error and discard the data
              fprintf(stderr, "Invalid data: %f\n", data[i]);
              pastError = true;//Note that an error has occured
              break;
            }
          }

          if(count >= FFTSIZE * groupSize){ //If we have enough data to calculate fft, create thread and calculate fft
              pthread_t thread_id; 
              pthread_create(&thread_id, NULL, calcfft, (void *)buff); //Create thread to calculate fft
              pthread_detach(thread_id); //Detach thread so it can be cleaned up
              count = 0;//Reset count
          }

          if(count == 0){//If we have sent all the data, reallocate memory for data
              buff = (float *)calloc(FFTSIZE * groupSize, sizeof(float) * 2);//Allocate memory for data
          }

          if (verbose) {
            fprintf(stderr, 
              "Received packet: %zu samples, %.f full secs, %f frac secs\n",
              num_rx_samps,
              difftime(full_secs, (int64_t)0),
              frac_secs);
              printf("%d\n", count);
          }
        }
    }

// Cleanup
close_file:
    fclose(fp);

free_buffer:
    if (buff) {
        if (verbose) {
            fprintf(stderr, "Freeing buffer.\n");
        }
        free(buff);
    }
    buff      = NULL;
    buffs_ptr = NULL;

free_rx_streamer:
    if (verbose) {
        fprintf(stderr, "Cleaning up RX streamer.\n");
    }
    uhd_rx_streamer_free(&rx_streamer);

free_rx_metadata:
    if (verbose) {
        fprintf(stderr, "Cleaning up RX metadata.\n");
    }
    uhd_rx_metadata_free(&md);

free_usrp:
    if (verbose) {
        fprintf(stderr, "Cleaning up USRP.\n");
    }
    if (return_code != EXIT_SUCCESS && usrp != NULL) {
        uhd_usrp_last_error(usrp, error_string, 512);
        fprintf(stderr, "USRP reported the following error: %s\n", error_string);
    }
    uhd_usrp_free(&usrp);

free_option_strings:
    if (device_args) {
        free(device_args);
    }

    fprintf(stderr, (return_code ? "Failure\n" : "Success\n"));
    return return_code;
}