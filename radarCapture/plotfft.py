import numpy as np
import matplotlib.pyplot as plt
import sys

args = sys.argv
plt.rcParams['font.size'] = 14

chunkSize = 2048 # number of samples per chunk
groupSize = int(args[1]) # number of chunks per group
Fs = int(float(args[2])) # sample rate
Ts = 1/(Fs * 1e6) # sample period
#print("Sample period: ", Ts, " seconds", " | Length of group: ", chunkSize * groupSize * Ts, " seconds")
C = int(float(args[3])) # center frequency
M = int(1e6) # megahertz
fileTime = int(args[4])*60 # time of file in seconds

data = np.fromfile(args[5], np.double)
logData = 10*np.log10(data)

N = len(logData)
numGroups = N//chunkSize #Calculate the number of groups of 2048 samples
calculatedTime = N * groupSize * Ts #Calculate the time of the file in seconds

logData = logData.reshape(numGroups, chunkSize) #Reshape the data into groups of 2048 sample

f = np.arange((C-(Fs/2)), (C+(Fs/2)), Fs/chunkSize) # frequency vector
t = np.arange(0, calculatedTime, calculatedTime/(N/chunkSize)) # time vector

xdim = len(f)-1
ydim = len(t)-1
print(xdim, ydim)

plt.rcParams["figure.figsize"] = (12,8)
pc = plt.pcolormesh(f, t, logData[:xdim, :ydim], shading='gouraud', cmap='jet')
cbar = plt.colorbar(pc)
cbar.set_label('dB', rotation=270)
plt.title("PSD " + args[5] + "\n" + args[2] + " MHz Bandwidth, " + args[3] + " MHz Center Frequency, " + args[4] + " min recording")
plt.xlabel("Frequency [MHz]")
plt.ylabel("Time [s]")

if len(args) > 6:
    plt.savefig(args[6], bbox_inches='tight')
else:
    plt.show()

