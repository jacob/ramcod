#!/usr/bin/python

"""
Profile for operation of the RAMCOD system on the POWDER platform.
It instantiates a rooftop node pair with an SDR and a compute node.
Additionally, it can connect the profile to a dataset if desired.

Instructions:
## Instructions for RAMCOD operation:
1. Choose the rooftop node type from the dropdown menu based on node availablity and desired sample location.
2. Choose the compute node type from the dropdown menu a d740 is recommended for RAMCOD operation.

For RAMCOD operation, the profile will instantiate a rooftop node with an SDR and a compute node.
The compute node will be connected to the SDR and the profile will install the necessary software.
1. Go to the directory with the ramcod software and run the following command:
    ```
    cd /local/repository
    ```
2. Run the following command to setup the node:
    ```
    sudo bash startupd740.sh
    ```
3. Run the following command to change to the RAMCOD directory:
    ```
    cd /local/repository/ramcod
    ```
3. Run the following command to start the RAMCOD server on the compute node for monitoring the SLC ASR-9:
    ```
    python3 server.py -s asr.csv
    ```
4. Run the following command in a new terminal window to start the RAMCOD radio interface:
    ```
    cd /local/repository/ramcod
    ```
    ```
    gcc radio.c  -lfftw3 -lm -luhd -pthread && ./a.out -f 2835e6 -r 100e6 -g 10
    ```

### NOTES For Dynamic Capture
- To monitor KMTX, the server.py command should be run with the -s kmtx.csv flag. And the radio interface should be started with the -f 2750e6 flag.
- While RAMCOD has shown to generally give good results at 100 Msamples/s, occasionally this high of a sample rate results in lots of overruns and dropped samples. If this is the case, try reducing the sample rate to 50 Msamples/s or 25 Msamples/s. Or restart the experiment with a different compute radio pair.

## Instructions for Static Sample Collection:
1. Choose the rooftop node type from the dropdown menu based on node availablity and desired sample location.
2. Choose the compute node type from the dropdown menu a d840 is required for static RX collection.
3. Optionally, choose a dataset to connect to the profile for long term storage of the samples.

For static sample collection, the profile will instantiate a rooftop node with an SDR and a compute node.
The compute node will be connected to the SDR and the profile will install the necessary software.
1. Copy the code into the nvme directory:
    ```
    sudo cp /local/repository/radioCapture /nvme
    ```
2. Go to the directory with the ramcod software and run the following command:
    ```
    cd /nvme
    ```
3. Run the following command to setup the node:
    ```
    sudo bash startupd840.sh
    ```
4. Run the following command to change to the radioCapture directory:
    ```
    cd /nvme/radioCapture
    ```
5. Open x310.sh and modify the frequency, gain, and sample rate as needed:
    ```
    nano x310.sh
    ```
6. Run the following command to start the radio capture:
    ```
    sudo bash x310.sh
    ```

### NOTES For Static Sample Collection
- The frequency, gain, and sample rate can be modified in the x310.sh file to capture the desired signal.
- If you taking a long term capture at a high sample rate it is recommended to run ```sudo fstrim /nvme``` to prevent performance degradation.
- Large samples can take a long time to transfer to the dataset so plan experiment time accordingly.
"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.emulab.spectrum as spectrum


setup_command = "/local/repository/startupd840.sh"
disk_image = \
        "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"


# Add a dataset to the profile
def add_dataset(dataset_urn, iface, fslink, count):
    fsnode = request.RemoteBlockstore("fsnode-%d" % count, "/mydata%d" % count)
    fsnode.dataset = dataset_urn
    
    fslink.addInterface(fsnode.interface)
    

pc = portal.Context()
request = pc.makeRequestRSpec()
request.initVNC()

pc.defineParameter(
    "node", #variable
    "Rooftop Node", # name shown
    portal.ParameterType.STRING, 
    "cellsdr1-bes", # choice shown
    [("cellsdr1-bes", "BEH-CELL"), ("cellsdr1-bes", "HOSPITAL-CELL"), 
        ("cellsdr1-meb", "MEB-CELL")], # options
    "Choose the rooftop radio for radar collection" # explanation
)

portal.context.defineParameter(
    "computetype", #variable
    "Compute node type", # name shown
    portal.ParameterType.STRING, 
    "d740", # choice shown
    ["d740","d840"], # options
    "d740 recommended for RAMCOD operation and d840 for static RX collection", # explanation
)

portal.context.defineStructParameter(
    "datasets", 
    "Connected datasets", [],
    multiValue=True,
    min=0,
    multiValueTitle="Datasets.",
    members=[
        portal.Parameter(
            "dataset",
            "POWDER dataset URN",
            portal.ParameterType.STRING,
            "urn:publicid:IDN+emulab.net:portalprofiles+ltdataset+DemoDataset",
        )
    ])
                   
                   
params = pc.bindParameters()
pc.verifyParameters()

node = request.RawPC("%s-node" % params.node)
node.hardware_type = params.computetype
node.disk_image = disk_image
node_radio_if = node.addInterface("usrp_if")
node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1", "255.255.255.0"))
radio_link = request.Link("%s-link" % params.node)
radio_link.addInterface(node_radio_if)
radio = request.RawPC("%s-radio"% params.node)
radio.component_id = params.node
radio_link.addNode(radio)
node.startVNC()
node.addService(rspec.Execute(shell="bash", command=setup_command))

if len(params.datasets) > 0:
    count = 0
    iface = node.addInterface("dataset_if")
    fslink = request.Link("fslink-%d" % count)
    fslink.best_effort = True
    fslink.vlan_tagging = True
    fslink.addInterface(iface)
    for dataset in params.datasets:
        add_dataset(dataset.dataset, node_radio_if, fslink, count)
        count += 1

portal.context.printRequestRSpec()